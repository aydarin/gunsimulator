import UIKit


class EmptyGunState: GunState {
    private let feedbackContext: FeedbackContext
    private let configuration: GunConfiguration
    
    init(feedbackContext: FeedbackContext, configuration: GunConfiguration) {
        self.feedbackContext = feedbackContext
        self.configuration = configuration
    }
    
    func trigger() -> GunState? {
        feedbackContext.dryShot()
        return nil
    }
    
    func recharge() -> GunState? {
        feedbackContext.reload()
        return ChargedWithoutBulletGunState(feedbackContext: feedbackContext, configuration: configuration)
    }
    
    func pullCharger() -> GunState? {
        feedbackContext.pullCharger()
        return PulledChargerEmptyGunState(feedbackContext: feedbackContext, configuration: configuration)
    }
    
    func putChargerBack() -> GunState? {
        return nil
    }
}
