import UIKit


class GunConfiguration {
    let bulletsCount = 10
    let isRechargingAvailable: Bool
    
    init(isRechargingAvailable: Bool) {
        self.isRechargingAvailable = isRechargingAvailable
    }
}
