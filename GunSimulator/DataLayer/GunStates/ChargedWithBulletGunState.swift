import UIKit


class ChargedWithBulletGunState: GunState {
    private let feedbackContext: FeedbackContext
    private let configuration: GunConfiguration
    
    private var bulletsCount: Int
    
    init(feedbackContext: FeedbackContext, configuration: GunConfiguration, bulletsCount: Int? = nil) {
        self.feedbackContext = feedbackContext
        self.configuration = configuration
        self.bulletsCount = bulletsCount ?? configuration.bulletsCount
    }
    
    func trigger() -> GunState? {
        if configuration.isRechargingAvailable {
            bulletsCount -= 1
        }
        
        feedbackContext.shot()
        return (bulletsCount == 0)
            ? EmptyGunState(feedbackContext: feedbackContext, configuration: configuration)
            : nil
    }
    
    func recharge() -> GunState? {
        feedbackContext.reload()
        return ChargedWithoutBulletGunState(feedbackContext: feedbackContext,
                                            configuration: configuration,
                                            bulletsCount: bulletsCount)
    }
    
    func pullCharger() -> GunState? {
        if configuration.isRechargingAvailable {
            bulletsCount -= 1
        }
        
        feedbackContext.pullCharger()
        return (bulletsCount == 0)
            ? PulledChargerEmptyGunState(feedbackContext: feedbackContext, configuration: configuration)
            : PulledChargerChargedGunState(feedbackContext: feedbackContext,
                                           configuration: configuration,
                                           bulletsCount: bulletsCount)
    }
    
    func putChargerBack() -> GunState? {
        return nil
    }
}
