import Foundation


protocol GunState {
    func trigger() -> GunState?
    func recharge() -> GunState?
    func pullCharger() -> GunState?
    func putChargerBack() -> GunState?
}
