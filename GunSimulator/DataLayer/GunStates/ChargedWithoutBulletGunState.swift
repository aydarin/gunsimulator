import UIKit


class ChargedWithoutBulletGunState: GunState {
    private let feedbackContext: FeedbackContext
    private let configuration: GunConfiguration
    
    private var bulletsCount: Int
    
    init(feedbackContext: FeedbackContext, configuration: GunConfiguration, bulletsCount: Int? = nil) {
        self.feedbackContext = feedbackContext
        self.configuration = configuration
        self.bulletsCount = bulletsCount ?? configuration.bulletsCount
    }
    
    func trigger() -> GunState? {
        feedbackContext.dryShot()
        return nil
    }
    
    func recharge() -> GunState? {
        feedbackContext.reload()
        return ChargedWithoutBulletGunState(feedbackContext: feedbackContext,
                                            configuration: configuration,
                                            bulletsCount: bulletsCount)
    }
    
    func pullCharger() -> GunState? {
        feedbackContext.pullCharger()
        return PulledChargerChargedGunState(feedbackContext: feedbackContext,
                                            configuration: configuration,
                                            bulletsCount: bulletsCount)
    }
    
    func putChargerBack() -> GunState? {
        return nil
    }
}
