import UIKit


class Gun {
    private let configuration: GunConfiguration
    
    private var state: GunState

    init(configuration: GunConfiguration) {
        self.configuration = configuration
        state = ChargedWithBulletGunState(feedbackContext: GunFeedbackContext(),
                                          configuration: configuration)
    }
    
    func trigger() {
        if let newState = state.trigger() {
            state = newState
        }
    }
    
    func recharge() {
        if let newState = state.recharge() {
            state = newState
        }
    }
    
    func pullCharger() {
        if let newState = state.pullCharger() {
            state = newState
        }
    }
    
    func putChargerBack() {
        if let newState = state.putChargerBack() {
            state = newState
        }
    }
}
