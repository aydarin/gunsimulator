import UIKit
import CoreMotion
import RxSwift


class ShakeMotionHandler {

    private let manager = CMMotionManager()
    private let shakeSubject = PublishSubject<CMDeviceMotion>()
    
    static let instance = ShakeMotionHandler()
    
    lazy var shake: Observable<CMDeviceMotion> = self.shakeSubject.asObservable()
    var isAvalable: Bool {
        return manager.isDeviceMotionAvailable
    }
    
    private init() {
        if isAvalable {
            manager.deviceMotionUpdateInterval = 0.02
            manager.startDeviceMotionUpdates(to: OperationQueue.main) {
                [weak self] data, error in
                
                if let data = data, data.userAcceleration.x < -2.5 {
                    self?.shakeSubject.onNext(data)
                }
            }
        }
    }
    
    deinit {
        shakeSubject.onCompleted()
    }
    
}
