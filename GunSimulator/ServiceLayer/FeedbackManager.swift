import UIKit
import AudioToolbox
import Device


class FeedbackManager {
    
    private let hapticAvailable: Bool
    
    private let shotFeedback = UIImpactFeedbackGenerator(style: .heavy)
    private let reloadBackFeedback = UIImpactFeedbackGenerator(style: .medium)
    private let reloadForwardFeedback = UIImpactFeedbackGenerator(style: .heavy)
    
    static let instance = FeedbackManager()
    
    private init() {
        let version = Device.version()
        hapticAvailable = (version == .iPhone7) || (version == .iPhone7Plus)
    }

    func shot() {
        if (hapticAvailable) {
            shotFeedback.impactOccurred()
        } else {
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        }
    }
    
    func putChargerBack() {
        reloadBackFeedback.impactOccurred()
    }
    
    func pullCharger() {
        reloadForwardFeedback.impactOccurred()
    }
}
