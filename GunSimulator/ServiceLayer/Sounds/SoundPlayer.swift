import Foundation
import AVFoundation


class SoundPlayer: NSObject, AVAudioPlayerDelegate {
    
    let soundURL: NSURL
    private var players: [AVAudioPlayer] = []
    
    init(soundURL: NSURL) {
        self.soundURL = soundURL
    }
    
    func play() {
        let soundPlayer = try! AVAudioPlayer(contentsOf: soundURL as URL)
        soundPlayer.delegate = self
        players.append(soundPlayer)
        
        soundPlayer.prepareToPlay()
        soundPlayer.play()
    }
    
    // MARK: AVAudioPlayerDelegate
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if let index = players.index(of: player) {
            players.remove(at: index)
        }
    }
}
