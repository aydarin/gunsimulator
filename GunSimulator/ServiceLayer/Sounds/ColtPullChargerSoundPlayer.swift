import UIKit


class ColtPullChargerSoundPlayer: SoundPlayer {
    init() {
        super.init(soundURL: NSURL(fileURLWithPath: Bundle.main.path(forResource: "colt-reload-forward", ofType: "wav")!))
    }
}
