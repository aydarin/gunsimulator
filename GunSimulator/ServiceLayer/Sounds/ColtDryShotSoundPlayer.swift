import UIKit


class ColtDryShotSoundPlayer: SoundPlayer {
    init() {
        super.init(soundURL: NSURL(fileURLWithPath: Bundle.main.path(forResource: "colt-dry-shot", ofType: "wav")!))
    }
}
