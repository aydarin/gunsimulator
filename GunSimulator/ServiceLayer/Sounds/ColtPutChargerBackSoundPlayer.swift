import UIKit


class ColtPutChargerBackSoundPlayer: SoundPlayer {
    init() {
        super.init(soundURL: NSURL(fileURLWithPath: Bundle.main.path(forResource: "colt-reload-backward", ofType: "wav")!))
    }
}
