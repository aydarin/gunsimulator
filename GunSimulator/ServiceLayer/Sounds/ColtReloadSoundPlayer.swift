import UIKit


class ColtReloadSoundPlayer: SoundPlayer {
    init() {
        super.init(soundURL: NSURL(fileURLWithPath: Bundle.main.path(forResource: "colt-fill-bullets", ofType: "wav")!))
    }
}
