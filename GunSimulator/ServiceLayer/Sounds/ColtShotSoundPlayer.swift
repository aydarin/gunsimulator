import Foundation
import AVFoundation


class ColtShotSoundPlayer: SoundPlayer {
    init() {
        super.init(soundURL: NSURL(fileURLWithPath: Bundle.main.path(forResource: "colt-shot", ofType: "wav")!))
    }
}
