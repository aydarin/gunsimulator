import UIKit


class GunFeedbackContext: FeedbackContext {
    
    private let shotPlayer = ColtShotSoundPlayer()
    private let pullChargerSoundPlayer = ColtPullChargerSoundPlayer()
    private let putChargerBackSoundPlayer = ColtPutChargerBackSoundPlayer()
    private let dryShotPlayer = ColtDryShotSoundPlayer()
    private let reloadSoundPlayer = ColtReloadSoundPlayer()
    
    private let feedbackManager = FeedbackManager.instance
    
    func shot() {
        shotPlayer.play()
        feedbackManager.shot()
    }
    
    func dryShot() {
        dryShotPlayer.play()
    }
    
    func pullCharger() {
        pullChargerSoundPlayer.play()
        feedbackManager.putChargerBack()
    }
    
    func putChargerBack() {
        putChargerBackSoundPlayer.play()
        feedbackManager.putChargerBack()
    }
    
    func reload() {
        reloadSoundPlayer.play()
    }
}
