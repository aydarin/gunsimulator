import Foundation


protocol FeedbackContext {
    func shot()
    func dryShot()
    func pullCharger()
    func putChargerBack()
    func reload()
}
