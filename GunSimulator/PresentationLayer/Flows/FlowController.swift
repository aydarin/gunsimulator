import UIKit


class FlowController {
    
    let navigationController: UINavigationController
    
    init() {
        navigationController = UINavigationController()
    }
    
    func start() -> UIViewController {
        navigationController.setViewControllers([configuredGunVC()], animated: false)
        return navigationController
    }
    
    private func configuredGunVC() -> GunVC {
        let vc = R.storyboard.main.gunScreen()!
        vc.viewModel = GunVM()
        
        return vc
    }

}
