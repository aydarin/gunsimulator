import UIKit
import RxSwift


class GunForceTouchHandlerView: UIView {
    
    var forceTouchAvailable: Bool {
        return (traitCollection.forceTouchCapability == .available)
    }
    
    private let touchForcedVariable = Variable(false)
    lazy private(set) var touchForced: Observable<Bool> = self.touchForcedVariable.asObservable()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        updateTouchForced(withTouches: touches)
    }
    
    override func touchesEstimatedPropertiesUpdated(_ touches: Set<UITouch>) {
        updateTouchForced(withTouches: touches)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        updateTouchForced(withTouches: touches)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        updateTouchForced(withTouches: touches)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        updateTouchForced(withTouches: touches)
    }
    
    private func updateTouchForced(withTouches touches: Set<UITouch>) {
        if forceTouchAvailable {
            touchForcedVariable.value = (touches.first(where: { $0.force > 5.0 }) != nil)
        }
    }
}
