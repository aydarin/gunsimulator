import UIKit
import RxSwift


class GunVM {
    
    private let disposeBag = DisposeBag()
    
    private let touchForcedVariable = Variable(false)
    private let gun = Gun(configuration: GunConfiguration(isRechargingAvailable: ShakeMotionHandler.instance.isAvalable))
    
    var touchForced: Bool {
        set {
            touchForcedVariable.value = newValue
        }
        get {
            return touchForcedVariable.value
        }
    }
    
    init() {
        touchForcedVariable.asObservable()
            .distinctUntilChanged()
            .filter({ $0 == true })
            .subscribe(onNext: { [weak self] _ in
                self?.gun.trigger()
            }).addDisposableTo(disposeBag)
        
        ShakeMotionHandler.instance.shake.subscribe { [weak self] _ in
            self?.recharge()
        }.addDisposableTo(disposeBag)
    }
    
    private func recharge() {
        gun.recharge()
    }
    
    // MARK: Actions
    
    func pullCharger() {
        gun.pullCharger()
    }
    
    func putChargerBack() {
        gun.putChargerBack()
    }

    func shotPressed() {
        gun.trigger()
    }
}
