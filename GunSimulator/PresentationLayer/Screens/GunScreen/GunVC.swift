import UIKit
import Rswift
import RxSwift
import RxCocoa


class GunVC: UIViewController {
    
    private let disposeBag = DisposeBag()
    
    var viewModel: GunVM!
    
    @IBOutlet private weak var gunImageView: UIImageView!
    @IBOutlet private weak var shotButton: UIButton!
    @IBOutlet private weak var barrelView: UIView!
    @IBOutlet private weak var forceTouchHandlerView: GunForceTouchHandlerView!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true
        
        updateShotButton()
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(gesture:)))
        barrelView.addGestureRecognizer(pan)
        
        forceTouchHandlerView.touchForced.subscribe(onNext: { [weak self] forced in
            self?.viewModel.touchForced = forced
        }).addDisposableTo(disposeBag)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        updateShotButton()
    }

    private func updateShotButton() {
        shotButton.isHidden = forceTouchHandlerView.forceTouchAvailable
        forceTouchHandlerView.isHidden = !forceTouchHandlerView.forceTouchAvailable
    }

    // MARK: Actions
    
    @IBAction func shotPressed(_ sender: Any) {
        viewModel.shotPressed()
    }
    
    // MARK: Reload pan gesture
    
    @objc private func handlePan(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            viewModel.pullCharger()
        case .ended, .cancelled:
            viewModel.putChargerBack()
        default: break
        }
    }
}
